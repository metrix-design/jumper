const rall = require("require.all"),
    fs = require("fs"),
    tls = require("tls"),
    httpProxy = require('http-proxy'),
    https = require('https'),
    http = require('http'),
    config = require("./config.json"),
    vhosts = rall(config.servers.root),
    apps = {},
    servers = {}

config.servers.includes.forEach(function(e) {
    let conf = require(`${config.servers.root}/${e}`)
    addVirtualHost(conf)
})

Object.keys(apps).map(port => {
    if (apps[port].protocol == "https") {
        let proxy
        var secureContext = {}
        Object.keys(apps[port].vhosts).map(hostext => {
            Object.keys(apps[port].vhosts[hostext]).map(vhost => {
                apps[port].vhosts[hostext].hosts.forEach(function(e) {
                    return secureContext[`${e}`] = tls.createSecureContext({
                        cert: fs.readFileSync(apps[port].vhosts[hostext].ssl.cert),
                        key: fs.readFileSync(apps[port].vhosts[hostext].ssl.key)
                    })
                })
            })
        })
        let ssl = {
            SNICallback: function (domain, cb) {
                let ctx = `${domain.split(":").splice(domain.split(":").findIndex(function() {return  domain.split(":").pop()}))[0]}`
                if (secureContext[ctx]) {
                    cb(null, secureContext[ctx])
                } else {
                    console.log(`${domain} specifies HTTPS as its protocol but does not provide a valid certificate.`)
                }
            },
            cert: fs.readFileSync('./ssl/selfsigned.crt'),
            key: fs.readFileSync('./ssl/selfsigned.key')
        }
        servers[port] = https.createServer(ssl, function (req, res) {
            Object.keys(apps[port].vhosts).map(vhost => {
                apps[port].vhosts[vhost].hosts.forEach(function(domain) {
                    if (!["80", "443"].indexOf(apps[port].vhosts[vhost].port) > -1) reqhost = req.headers["host"].split(":").splice(req.headers["host"].split(":").findIndex(function() {return req.headers["host"].split(":").pop()}))[0]
                    if (reqhost == domain) {
                        let ip = cleanAddress(req.connection.remoteAddress)
                        let server = Math.round(parseInt(ip.split(".")[0]) / 255 * config.workers.count)
                        proxy = new httpProxy.createProxyServer({
                            target: {
                                host: `localhost`,
                                port: config.workers.portstart + parseInt(`${server}00`) + parseInt(apps[port].vhosts[vhost].hostext)
                            }
                        })
                        proxy.on('proxyReq', function(proxyReq, req, res, options) {
                            proxyReq.setHeader('x-forwarded-for', req.connection.remoteAddress);
                        })
                        proxy.web(req, res, {ws: true, ssl});
                    }
                })
            })
        }).on("upgrade", function (req, socket, head) {
            proxy.ws(req, socket, head)
        })
    } else {
        let proxy
        servers[port] = http.createServer(function (req, res) {
            Object.keys(apps[port].vhosts).map(vhost => {
                apps[port].vhosts[vhost].hosts.forEach(function(domain) {
                    if (!["80", "443"].indexOf(apps[port].vhosts[vhost].port) > -1) reqhost = req.headers["host"].split(":").splice(req.headers["host"].split(":").findIndex(function() {return req.headers["host"].split(":").pop()}))[0]
                    if (reqhost == domain) {
                        let ip = cleanAddress(req.connection.remoteAddress)
                        let server = Math.round(parseInt(ip.split(".")[0]) / 255 * config.workers.count)
                        proxy = new httpProxy.createProxyServer({
                            target: {
                                host: `localhost`,
                                port: config.workers.portstart + parseInt(`${server}00`) + parseInt(apps[port].vhosts[vhost].hostext)
                            }
                        })
                        proxy.on('proxyReq', function(proxyReq, req, res, options) {
                            proxyReq.setHeader('x-forwarded-for', req.connection.remoteAddress);
                        })
                        proxy.web(req, res, {ws: true});
                    }
                })
            })
        }).on("upgrade", function (req, socket, head) {
            proxy.ws(req, socket, head)
        })
    }
})

Object.keys(servers).map(port => {
    servers[port].listen(port)
})

function cleanAddress(str) {
    if (str == "::1") return "127.0.0.1"
    if (str.indexOf(":ffff:") > -1) return str.split(":ffff:").pop()
}

function getPort(port, protocol) {
    if (!apps[port]) {
        apps[port] = {
            port,
            protocol,
            vhosts: {}
        }
    }
    return apps[port]
}
function addVirtualHost(conf) {
    if (!apps[conf.port]) getPort(conf.port, conf.protocol)
    apps[conf.port].vhosts[conf.hostext] = {
        hosts: conf.hosts,
        hostext: conf.hostext,
        port: conf.port,
        ssl: (conf.ssl) ? {
            cert: conf.root + conf.ssl.cert,
            key: conf.root + conf.ssl.key
        } : {}
    }
    return apps[conf.port].vhosts[conf.hostext];
}