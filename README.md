# Jumper

**PLEASE NOTE: YOU CAN NOT SERVE MULTIPLE HTTPS SITES ON LOCAL IPS DUE TO TLS/SNI LIMITATIONS**  

Jumper is a clustered, load-balancing, express-based webserver. It is written in Node and is fully scriptable.  

Please note that this guide was written for Linux. If you are using any other OS, you may need to adjust some shell commands. We will be creating more information on other operating systems.
This guide assumes that you got root access to the device you are installing jumper on, that the device has NodeJS v8 or newer and NPM installed, and that you got a basic understanding of the JSON syntax and the Linux environment.  

Jumper is licensed under the MIT license.

## Installation

To install jumper, run this shell command in the folder you want to install jumper in. The folder needs to be empty.
```
git clone https://gitlab.com/metrix-design/jumper.git . && npm install
```

## Setup

**Be sure to always add a trailing slash to file paths!** 

Once installed, you will need to set up jumper. As jumper is not a plug-and-play webserver, it will need some setup.
First, you need to create a config file: Create a new file in the jumper directory and call it "config.json".
Next, put the following text inside the config file: (example file)

```json
{
    "workers": {
        "portstart": 56100,
        "count": 8
    },
    "servers": {
        "root": "./vhosts/",
        "includes": [ 
            "localhost.json",
            "https-acorns.interesting.website.com.json"
        ]
    }
}
```

You should adjust the values to match your (future) configuration.  
Here is an explanation of the configuration:   
`workers.portstart`: What should the first port assigned to jumper be. As jumper requires lots of ports, set it to something above 20000.  
`workers.count`: Optional. How many workers to spawn in. If not set, defaults to CPU core count. I recommend you not to use this option.  
`servers.root`: The folder in which your virtual host configuration files will be located. Can be relative or absolute.  
`servers.includes`: An Array of virtual host files to use. The path is always relative to the virtual hosts folder.  

The next thing you will need to do is configure your first virtual host. To do that, create a file in your previously-chosen virtual hosts folder. I would recommend naming it as the (first) domain you will serve with that virtual host, the protocol in front plus ".json" at the end. So my imaginary virtual host would be named "https-acorns.interesting.website.com.json". You should put the following in it: (example file)

```json
{
    "hosts": ["acorns.interesting.website.com"],
    "root": "/var/jumper/",
    "path": "www/",
    "port": "443",
    "protocol": "http",
    "hostext": "01",
    "nmfolder": false,
    "scripts": "scripts/",
    "index": "index.html",
    "errorpage": "error.html",
    "ssl": {
        "cert": "ssl/cert.pem",
        "key": "ssl/key.pem"
    }
}
```

Again, adjust the configuration to your needs. You can have as many vhosts as you need. As a note, you can not have multiple virtual hosts in the same file, and to enable the virtual host, add the file's name to the main configuration's `servers.includes` array. This makes it easy to disable specific vhosts, but also harder to add new ones.  
Again, here are the details about the config... this one will be a tiny bit bigger in size, i fear:  
`hosts`: Array of domains you want this vhost to serve.  
`root`: The folder in which all of the files for this vhost are located. Should be absolute.  
`path`: Path relative to root in which the files to serve are located. `Ex. index.html, login.ejs, css/bootstrap.css, etc.`  
`port`: Which port should this vhost be served on. You should try to follow IANA's port list to choose good ports.  
`protocol`: Which protocol to use, HTTP or HTTPS. If you are using HTTP, you should not choose a port thats normally used for HTTPS. Again, check IANA's list.  
`nmfolder`: To or not to serve the node_modules folder in the vhost root. Set to true or false. The node\_modules folder is served always under /nm.
`static`: To or not to serve a static folder in the vhost root. Set to static folder name or to false. The static folder is served always under /static.
`errorpage`: An EJS HTML file that is served when an 500 or 404 occurs. Path relative to `root`. You can use the `error` parameter to print specific error pages by using EJS: `<%= error %>`. This parameter outputs the error code as a Number.  
`hostext`: Which port to use on the workers. More on that later.  
`scripts`: Optional. A directory path relative to the vhost root which contains script files you might want to load for your website, such as larger scripts like login code which you would probably not want to code inline with the HTML code.  
`index`: Optional. A custom index file name. The file needs to be inside, or nested inside of the `path`. Use this if your index file is not named `index.html`  
`ssl`: Only needed if using HTTPS. Paths are relative to the vhost root.  

You should be all setup now.  

## Running

If you followed my guide, you should now have a valid jumper configuration. To start it for testing, open your Terminal and navigate to the directory you installed jumper in. Then, run the following command.

```
npm start
```

This should start jumper.  
However, as soon as you close the Terminal/close the SSH session, jumper will stop aswell. This is why, in a production environment, you should run jumper with PM2, a simple to use Process Manager written in NodeJS aswell. To install PM2, run the following command: 

```
sudo npm i -g pm2
```

That will install PM2 into the PATH, so you can run it from anywhere on the system.  
  
To start jumper with PM2, open a Terminal in the directory you installed jumper in, and run the following command: 

```
pm2 start main.js --name jumper
```

Feel free to change the name to anything you want.  
You should now have jumper running in the background. To confirm that it is running, check the output from this command: 

```
pm2 list
```

If jumper is showing as errored, then you should check your configuration for any typos or mistakes.  
  
Now, if you want to stop or restart jumper for some reason, you can use these commands respectevly: 

```
pm2 stop jumper
```

and  

```
pm2 restart jumper
```

Again, if you changed the process name when starting jumper, you will need to change it here aswell.  

## Frequently Asked Questions

#### Whats all of this "lots of ports" nonsense? Why would jumper need lots of ports?

Because jumper summons multiple workers, to make proxying easier, each worker has its own 100 port range. So if you set the `workers.portstart` property in the config to  56100, and you had 8 workers, jumper would require 800 ports. That is why you should choose a really high port number. That might seem very inefficient, and is also a limiting factor as you can not have over 99 vhosts on a single jumper setup, but it makes stuff much easier to build up on, and jumper is all about tweakability.

#### What is the hostext thingy?

You should set it to a number from 1 to 99. Also make sure that no vhost has the same hostext as any other vhost. That field is required and only used internally to make proxying easier. If set up correctly, this should have no effect on the actual website.

### What is that IANA port list you are speaking of? Can i get a link to it?
Sure, here it is... wait no, google it!  
Just kidding, [here is the real link](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml).

### How can i use the scripts thing? What is it for?
It was intended for more advanced users. It basically allows you to write big chunks of code and instead of messing with EJS inline code, you put it in a .js file in the scripts folder for that vhost. Any .js file in that folder is imported and executed. The code gets executed inside of the request handler, so it provides the req and res function. Here is an example script, that takes over the "/" route: 

```js
module.exports = function(options) {
    app = options.app
    req = options.req
    res = options.res
    next = options.next
    if (req.path == "/") {
        res.send("This route is handled by a script")
    }
}
```