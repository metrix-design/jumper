const cluster = require('cluster'),
    config = require("./config.json")
    server = require('./server.js'),
    os = require('os'),
    worker = {}

if (cluster.isMaster) {
    let workersalive = 0
    let workersindex = 0
    let workerscount = parseInt(config.workers.count) || (os.cpus().length < 2 ? 2 : os.cpus().length)
    if (workersalive != workerscount) {
        for (var i = 0; i < workerscount; i++) {
            worker[workersindex] = cluster.fork()
            workersindex++
        }
    }
    setTimeout(e => {
        console.log((workersalive >= workerscount) ? `All workers started successfully. ${workersalive} workers running.` : `${workersalive} workers alive. Trying to create ${workerscount - workersalive} new workers.`)
        if (workersalive != workerscount) {
            for (var i = 0; i < workerscount; i++) {
                worker[workersindex] = cluster.fork()
                workersindex++
            }
            setTimeout(e => {console.log((workersalive >= workerscount) ? `All workers started successfully. ${workersalive} workers running.` : `${workersalive} workers alive. Trying to create ${workerscount - workersalive} new workers.`)}, 1000)
        }
    }, 1000)
    for (var i = 0; i < workerscount; i++) {
        worker[i].on("message", msg => {
            if (msg.alive) workersalive++
            if (msg.toomanyports) {
                console.log("Too many ports used by jumper. jumper can only serve sites on up to 99 different ports.")
                process.exit()
            }
        })
        worker[i].send({port: config.workers.portstart + parseInt(`${i}00`)})
    }
}


