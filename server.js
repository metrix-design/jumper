const rall = require("require.all")
const fs = require("fs")
const express = require("express")
const app = {}
const config = require("./config")
const vhosts = rall(config.servers.root)
const cluster = require("cluster")
const http = require("http")
const https = require("https")

config.servers.includes.forEach(function(element) {
    let conf = require(`${config.servers.root}/${element}`)
    const name = conf.port + "/" + conf.hostext
    if (!conf.root) conf.root = __dirname
    let ssl = (conf.protocol == "https") ? {cert: fs.readFileSync(conf.root + conf.ssl.cert), key: fs.readFileSync(conf.root + conf.ssl.key)} : {}
    app[name] = express()
    let thisapp = app[name]
    thisapp.engine('html', require('ejs').renderFile)
    thisapp.set('view engine', 'html')
    thisapp.set('views', conf.root + conf.path);
    if (conf.nmfolder) thisapp.use(`${conf.root}node_modules`, express.static("/nm"));
    if (conf.staticfolder) thisapp.use(conf.root + conf.staticfolder, express.static("/static"));
    thisapp.conf = conf
    thisapp.server = (conf.protocol == "https") ? https.createServer(ssl, thisapp) : http.createServer(thisapp)
    thisapp.all("*", (req, res, next) => {
        if (conf.hosts.indexOf(req.hostname) > -1) {
            let client_ip = req.headers['x-forwarded-for']
            res.setHeader("x-powered-by", "jumper/0.0.1")
            req.clientip = client_ip
            if (conf.scripts) rall(conf.root + conf.scripts)({app: thisapp, req, res, next});
            if (res.headersSent) return;
            if (conf.index && req.path == "/") {
                res.render(conf.index, {thisapp, req, res, next})
            } else {
                if (fs.existsSync(conf.root + conf.path + "/" + req.path)) {
                    res.render(req.path, {thisapp, req, res, next})
                } else {
                    if (conf.errorpage) {
                        res.status(404).render(conf.errorpage, {thisapp, req, res, next, error: 404})
                    } else {
                        res.status(404).send("404: Not found.")
                    }
                }
            }
        } 
    })
    thisapp.use((err, req, res, next) => {
        if (conf.hosts.indexOf(req.hostname) > -1) {
            let client_ip = req.headers['x-forwarded-for']
            res.setHeader("x-powered-by", "jumper/0.0.1")
            req.clientip = client_ip
            if (conf.scripts) rall(conf.root + conf.scripts)({app: thisapp, req, res, next});
            if (res.headersSent) return;
            if (err) {
                console.log(err)
                if (conf.errorpage) {
                    res.status(500).render(conf.errorpage, {thisapp, req, res, next, error: 500})
                } else {
                    res.status(500).send("500: Internal Server Error.")
                }
            }
        }
    })
})
if (cluster.isWorker) {
    process.send({alive: true})
}

process.on("message", msg => {
    Object.keys(app).map(key => {
        app[key].listen(msg.port + parseInt(app[key].conf.hostext), "127.0.0.1")
        if (Object.keys(app).length > 98 || app[key].conf.hostext > 99) process.send({toomanyports: true})
    })
})
